# KriptoMail
## E-mail Client with a Cryptographic Twist
---

KriptoMail is a simple Android email client app that able to encrypt and embed digital signature onto user's message.

### Features
1. Inbox
2. Sent mail storage
3. Public-private key generator
4. Message encryption
5. Digital signature

### Public-Private Key Generator
The application use asymmetric keys scheme to generate and validate every digital signature. The key pairs are generated using *Elliptic Curve Cryptography* algorithm.

### Encryption
Email encryptions are accomplished using **474 Algorithm**, a home-made cryptographic algorithm.

### Digital Signature
The application implement simple digital signature algorithm using the aforementioned asymmetric keys and hash function.

---

## Important Notes on Current Project States
- The application only works on GMail account.
- The application is recognized by Google as *less secure app*, so you have to allow less secure app access on your Google account. More info [here](https://support.google.com/accounts?p=lsa_blocked&hl=en).
- 
