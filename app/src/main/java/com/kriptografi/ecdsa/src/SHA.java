package com.kriptografi.ecdsa.src;

import static java.lang.Math.min;
import static java.lang.System.arraycopy;
import static java.util.Arrays.fill;
import java.io.ByteArrayOutputStream;
import java.math.BigInteger;

public class SHA{

    // For converting purpose
    private static final byte[] ENCODE_BYTE_TABLE = {
            (byte) '0', (byte) '1', (byte) '2', (byte) '3', (byte) '4', (byte) '5', (byte) '6', (byte) '7',
            (byte) '8', (byte) '9', (byte) 'a', (byte) 'b', (byte) 'c', (byte) 'd', (byte) 'e', (byte) 'f'
    };

    //hashing function. only this method can be called outside this class
    //input  : message, array of byte
    //output : hash code, string
    public String hash(final byte[] msg){
        int[] state = new int[200];
        int[] uMsg = convertToUint(msg);
        int rate = 1088;
        int cap = 512;
        int delSuffix = 0x06;
        int outputByteLen = 256/8;
        int rateInBytes = rate/8;
        int blockSize = 0;
        int inputOffset = 0;

        // Absorb
        while (inputOffset < uMsg.length) {
            blockSize = min(uMsg.length - inputOffset, rateInBytes);
            for (int i = 0; i < blockSize; i++) {
                state[i] = state[i] ^ uMsg[i+inputOffset];
            }
            inputOffset = inputOffset + blockSize;
            if (blockSize==rateInBytes) {
                KeccakF1600(state);
                blockSize = 0;
            }
        }

        // padding
        state[blockSize] = state[blockSize] ^ delSuffix;
        state[rateInBytes-1] = state[rateInBytes-1] ^ 0x80;
        KeccakF1600(state);

        // squeeze
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        while (outputByteLen > 0) {
            blockSize = min(outputByteLen,rateInBytes);
            for (int i = 0;i < blockSize; i++) {
                output.write((byte) state[i]);
            }

            outputByteLen -= blockSize;
            if (outputByteLen > 0) {
                KeccakF1600(state);
            }
        }

        return convertBytesToString(output.toByteArray());
    }

    // sponge construction
    private void KeccakF1600(final int[] state) {
        BigInteger[][] lanes = new BigInteger[5][5];

        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                int[] data = new int[8];
                arraycopy(state, 8*(i+5*j), data, 0, data.length);
                lanes[i][j] = convertLEto64(data);
            }
        }
        KeccakF1600Lanes(lanes);

        fill(state, 0);
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                int[] data = convert64ToLE(lanes[i][j]);
                arraycopy(data, 0, state, 8*(i+5*j), data.length);
            }
        }
    }

    // operation per lanes
    private void KeccakF1600Lanes(final BigInteger[][] state) {
        int r = 1;
        for (int round = 0; round < 24; round++) {
            BigInteger[] C = new BigInteger[5];
            BigInteger[] D = new BigInteger[5];

            for (int i = 0; i < 5; i++) {
                C[i] = state[i][0].xor(state[i][1]).xor(state[i][2]).xor(state[i][3]).xor(state[i][4]);
            }

            for (int i = 0; i < 5; i++) {
                D[i] = C[(i + 4) % 5].xor(lr64(C[(i + 1) % 5], 1));
            }

            for (int i = 0; i < 5; i++) {
                for (int j = 0; j < 5; j++) {
                    state[i][j] = state[i][j].xor(D[i]);
                }
            }

            int x = 1;
            int y = 0;
            BigInteger current = state[x][y];
            for (int i = 0; i < 24; i++) {
                int temp = x;
                x = y;
                y = (2 * temp + 3 * y) % 5;

                BigInteger shiftValue = current;
                current = state[x][y];

                state[x][y] = lr64(shiftValue, (i + 1) * (i + 2) / 2);
            }

            for (int j = 0; j < 5; j++) {
                BigInteger[] t = new BigInteger[5];
                for (int i = 0; i < 5; i++) {
                    t[i] = state[i][j];
                }

                for (int i = 0; i < 5; i++) {
                    state[i][j] = t[i].xor(t[(i + 1) % 5].not().and(t[(i + 2) % 5]));
                }
            }

            for (int i = 0; i < 7; i++) {
                r = ((r << 1) ^ ((r >> 7) * 0x71)) % 256;
                int bitPos = (1 << i) -1;
                if ((r & 2) != 0) {
                    state[0][0] = state[0][0].xor(new BigInteger("1").shiftLeft(bitPos));
                }
            }
        }
    }

    // convert array of byte to array of integer
    private int[] convertToUint(final byte[] data) {
        int[] converted = new int[data.length];
        for (int i = 0; i < data.length; i++) {
            converted[i] = data[i] & 0xFF;
        }
        return converted;
    }

    // convert 64 byte to Little Endian
    private int[] convert64ToLE(final BigInteger longint) {
        int[] data = new int[8];
        BigInteger mod256 = new BigInteger("256");
        for (int i = 0; i < 8; i++) {
            data[i] = longint.shiftRight((8 * i)).mod(mod256).intValue();
        }
        return data;
    }

    // convert Little Endian to 64 byte
    private BigInteger convertLEto64(final int[] data) {
        BigInteger longint = new BigInteger("0");
        for (int i = 0; i < 8; i++) {
            longint = longint.add(new BigInteger(Integer.toString(data[i])).shiftLeft(8 * i));
        }
        return longint;
    }

    // left rotate 64
    private BigInteger lr64(final BigInteger value, final int rotate) {
        BigInteger lp = value.shiftRight(64 - (rotate % 64));
        BigInteger rp = value.shiftLeft(rotate % 64);

        return lp.add(rp).mod(new BigInteger("18446744073709551616"));
    }

    // convert array of bytes to string
    private String convertBytesToString(final byte[] data) {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        for (int i = 0; i < data.length; i++) {
            int val = data[i] & 0xFF;
            buffer.write(ENCODE_BYTE_TABLE[(val >>> 4)]);
            buffer.write(ENCODE_BYTE_TABLE[val & 0xF]);
        }
        return new String(buffer.toByteArray());
    }
}
