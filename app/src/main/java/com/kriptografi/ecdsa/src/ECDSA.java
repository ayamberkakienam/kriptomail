package com.kriptografi.ecdsa.src;

import java.lang.Math;
import java.math.BigInteger;
import java.util.ArrayList;

public class ECDSA {

    private int a;
    private int b;
    private int p;
    private int n;
    private Point base;

    Point INF_O = new Point(Integer.MAX_VALUE, Integer.MAX_VALUE);

    public ECDSA(int a, int b, int p) {
        this.p = p;
        this.a = a;
        this.b = b;
        this.n = 0;
    }

    public int getP() {
        return p;
    }

    public void setP(int p) {
        this.p = p;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    public Point getBase() {
        return base;
    }

    public void setBase(Point base) {
        this.base = base;
    }


    public boolean isPrime(int number) {
        if (number <= 1) {
            return false;
        } else {
            int sqrt = (int) Math.sqrt(number) + 1;
            for (int i = 2; i < sqrt; i++) {
                if (number % i == 0) {
                    // number is perfectly divisible - no prime
                    return false;
                }
            }
            return true;
        }
    }

    public Point createPubKey(int priKey) {
        //priKey interval [1, n-1]
        OperationECC op = new OperationECC(getA(), getB(), getP());
        return op.multiply(priKey, getBase());
    }

    public void calculateN() {
        OperationECC op = new OperationECC(getA(), getB(), getP());
        ArrayList<Point> generatedPoint = op.generateGaloisField();

        Point temp = new Point(0,0);
        int i = 0;
        int n = 0;
        while (temp.getX() != INF_O.getX() && i < generatedPoint.size()) {
            n = 0;
            while (temp.getX() != INF_O.getX() || isPrime(n) == false) {
                n++;
                temp = op.multiply(n, generatedPoint.get(i));
            }
            i++;
        }
        setBase(generatedPoint.get(i-1));
        setN(n);
    }


    public ArrayList<Integer> generateSignature(int shaLeftBits, int priKey) {
        ArrayList<Integer> signature = new ArrayList<>();
        OperationECC op = new OperationECC(getA(), getB(), getP());
        int random;

        Point rPoint;
        long r = 0;
        long s = 0;
        int invK = 0;
        while (r == 0 || s == 0) {
            random = (int)(Math.random() * getN() + 1);
            while (BigInteger.valueOf(random).gcd(BigInteger.valueOf(getN())).intValue() != 1) {
                random = (int)(Math.random() * getN() + 1);
            }
            rPoint = op.multiply(random, getBase());
            r = BigInteger.valueOf(rPoint.getX()).mod(BigInteger.valueOf(getN())).intValue();
            invK = (BigInteger.valueOf(random).modInverse(BigInteger.valueOf(getN())).intValue());
            s = BigInteger.valueOf(invK * (shaLeftBits + r * priKey)).mod(BigInteger.valueOf(getN())).intValue();
        }

        signature.add(new Integer((int) r));
        signature.add(new Integer((int) s));
        return signature;
    }

    public boolean checkValidSignature(ArrayList<Integer> signature) {
        if (signature.get(0).intValue() < 1 || signature.get(0).intValue() > (getN()-1)) {
            return false;
        } else if (signature.get(1).intValue() < 1 || signature.get(1).intValue() > (getN()-1)) {
            return false;
        } else {
            return true;
        }
    }

    public boolean verifSignature(int shaLeftBits, ArrayList<Integer> signature, Point pubKey) {
        OperationECC op = new OperationECC(getA(), getB(), getP());
        boolean accepted;
        if (checkValidSignature(signature) == false) {
            accepted = false;
        } else {
            Point vPoint, u1Base, u2PubKey;
            int r, s, w, invS, u1, u2;
            long v;
            r = signature.get(0).intValue();
            s = signature.get(1).intValue();
            invS = (BigInteger.valueOf(s).modInverse(BigInteger.valueOf(getN())).intValue());
            w = BigInteger.valueOf(invS).mod(BigInteger.valueOf(getN())).intValue();
            u1 = BigInteger.valueOf(shaLeftBits * w).mod(BigInteger.valueOf(getN())).intValue();
            u2 = BigInteger.valueOf(r * w).mod(BigInteger.valueOf(getN())).intValue();
            u1Base = op.multiply(u1, getBase());
            u2PubKey = op.multiply(u2, pubKey);
            vPoint = op.add(u1Base, u2PubKey);
            v = BigInteger.valueOf(vPoint.getX()).mod(BigInteger.valueOf(getN())).intValue();
            if (v == r) {
                accepted = true;
            } else {
                accepted = false;
            }
        }
        return accepted;
    }

}
