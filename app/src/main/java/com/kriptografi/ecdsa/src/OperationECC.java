package com.kriptografi.ecdsa.src;

import java.math.BigInteger;
import java.util.ArrayList;

public class OperationECC {

    private int a;
    private int b;
    private int p;

    Point INF_O = new Point(Integer.MAX_VALUE, Integer.MAX_VALUE);

    public OperationECC(int a, int b, int p) {
        this.a = a;
        this.b = b;
        this.p = p;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    public int getP() {
        return p;
    }

    public void setP(int p) {
        this.p = p;
    }

    public boolean checkParams() {
        return (4* Math.pow(getA(), 3) + 27* Math.pow(getB(),2)) % getP() != 0 ? true : false;
    }

    public ArrayList<Point> generateGaloisField() {
        ArrayList<Point> points = new ArrayList<>();
        ArrayList<Integer> valsY;
        for (int i = 0; i < getP(); i++) {
            valsY = computeEquation(i);
            if (!valsY.isEmpty()){
                for (int j = 0; j < valsY.size(); j++) {
                    Point point = new Point(i, valsY.get(j));
                    points.add(point);
                }
            }
        }
        points.add(INF_O);
        return points;
    }

    public ArrayList<Integer> computeEquation(long x) {
        ArrayList<Integer> ys = new ArrayList<>();
        ArrayList<Integer> poweredTwo = new ArrayList<>();
        for (int i = 0; i < getP(); i++) {
            poweredTwo.add((int) Math.pow(i,2));
        }

        int y = (int) (Math.pow(x,3) + (getA() * x) + getB());

        for (int i = 0; i < getP(); i++) {
            if (poweredTwo.get(i) % getP() == y % getP()) {
                ys.add(i);
            }
        }
        return ys;
    }

    public Point add(Point p1, Point p2) {
        Point result = new Point(0,0);

        if (p1.getX() == 0 && p1.getY() == 0) {
            result.setX(p2.getX());
            result.setY(p2.getY());
        } else if (p2.getX() == 0 && p2.getY() == 0) {
            result.setX(p1.getX());
            result.setY(p1.getY());
        } else if (p1.getX() - p2.getX() == 0) {
            result.setX(Integer.MAX_VALUE);
            result.setY(Integer.MAX_VALUE);
        } else {
            long inv, lambda, xr, yr;
            inv = (BigInteger.valueOf(p2.getX() - p1.getX()).modInverse(BigInteger.valueOf(getP())).intValue());
            lambda = BigInteger.valueOf((p2.getY() - p1.getY()) * inv).mod(BigInteger.valueOf(getP())).intValue();
            xr = BigInteger.valueOf((int) Math.pow(lambda, 2) - p1.getX() - p2.getX()).mod(BigInteger.valueOf(getP())).intValue();
            yr = BigInteger.valueOf(lambda * (p1.getX() - xr) - p1.getY()).mod(BigInteger.valueOf(getP())).intValue();

            result.setX(xr);
            result.setY(yr);
        }
        return result;
    }

    public Point minus(Point p1, Point p2) {
        Point result;
        Point temp = new Point(0,0);

        long ytemp = BigInteger.valueOf(-p2.getY()).mod(BigInteger.valueOf(getP())).intValue();
        temp.setX(p2.getX());
        temp.setY(ytemp);

        result = add(p1, temp);

        return result;
    }

    public Point duplicate(Point p) {
        Point result = new Point(0,0);

        if (p.getY() != 0) {
            long lambda, inv, xr, yr;
            inv = (BigInteger.valueOf(2*p.getY()).modInverse(BigInteger.valueOf(getP())).intValue());
            lambda = BigInteger.valueOf((3*(int)Math.pow((p.getX()), 2) + getA()) * inv).mod(BigInteger.valueOf(getP())).intValue();
            xr = BigInteger.valueOf(((int) Math.pow(lambda, 2)) - 2*p.getX()).mod(BigInteger.valueOf(getP())).intValue();
            yr = BigInteger.valueOf((lambda * (p.getX() - xr)) - p.getY()).mod(BigInteger.valueOf(getP())).intValue();

            result.setX(xr);
            result.setY(yr);
        }

        return result;
    }

    public Point multiply(int k, Point p) {
        Point result = new Point(0,0);
        Point base = new Point(p.getX(), p.getY());

        String binary = Long.toBinaryString(k);
        for (int i=binary.length()-1; i>=0; i--) {
            if (binary.charAt(i) == '1') {
                if (i == binary.length()-1){
                    result.setX(base.getX());
                    result.setY(base.getY());
                } else {
                    Point restemp = add(result, base);
                    result.setX(restemp.getX());
                    result.setY(restemp.getY());
                }
            }
            Point basetemp = duplicate(base);
            base.setX(basetemp.getX());
            base.setY(basetemp.getY());
        }

        return result;
    }
}
