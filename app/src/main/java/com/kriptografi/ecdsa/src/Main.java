package com.kriptografi.ecdsa.src;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;


public class Main {
    public static void main(String[] args) {
        System.out.println("Hello!");
//        OperationECC op = new OperationECC(1,6,11);
//        ArrayList<Point> generatedpoint = op.generateGaloisField();
////        System.out.println(op.checkParams());
////        System.out.println(generatedpoint.size());
//        for (int i = 0; i < generatedpoint.size(); i++) {
//            System.out.println(generatedpoint.get(i).getX() + "," + generatedpoint.get(i).getY());
//        }

//        Point base = new Point(9,4);


//        System.out.println(ecceg.getN());
//        System.out.println(ecceg.getBase().getX() + "," + ecceg.getBase().getY());

//        System.out.println(ecceg.isPrime(0));
//        System.out.println(ecceg.isPrime(15));
//        System.out.println(ecceg.isPrime(17));

//        System.out.println(generatedpoint.get(0).getX() + "," + generatedpoint.get(0).getY());
//        Point PubKey = ecceg.createPubKey(5, generatedpoint.get(0));
//        String message = "abcdefghijklmnopqrstuvwxyz";
//        System.out.println("cipher");
//        String ciphertext = ecceg.encrypt(PubKey, message);
//        System.out.println("plain");
//        String plaintext = ecceg.decrypt(5, ciphertext);
//        System.out.println(plaintext);

//        ArrayList<Point> encodedpoint = ecceg.encode("abcdefghijklmnopqrstuvwxyz");

//        ArrayList<Point> valsGF = new ArrayList<>();
//        valsGF = ecceg.generateGaloisField();
//
//        System.out.println("Hasil");
//        for (int i = 0; i < encodedpoint.size(); i++) {
//            System.out.println(encodedpoint.get(i).getX() + "," + encodedpoint.get(i).getY());
//        }
//
//        System.out.println("Hasil Decode");
//        String res = new String(ecceg.decode(encodedpoint));
//        System.out.println(res);
//
//        ArrayList<Point> valsCopy = new ArrayList<>();
//        valsCopy = valsGF;
//        if (valsCopy.isEmpty()) {
//            System.out.println("Kosong Choy");
//        } else {
//            System.out.println(valsCopy.size());
//        }

//        Point p2 = new Point(5,9);
//        Point p3 = new Point(8,8);
//        Point resultadd = ecceg.add(p1, p2);
//        System.out.println(resultadd.getX() + "," + resultadd.getY());
//
//        for (int i =1; i <= 13; i++) {
//            Point resultduplicate = ecceg.multiply(i, p1);
//            System.out.println(resultduplicate.getX() + "," + resultduplicate.getY());
//        }
//
//        Point resultmin = ecceg.minus(p3, p1);
//        System.out.println(resultmin.getX() + "," + resultmin.getY());

        SHA sha = new SHA();
        // testing with string
        String text = "The binary system is a numerical system that functions virtually identically to the decimal number system that people are likely more familiar with. While the decimal number system uses the number 10 as its base, the binary system uses 2.";
        byte[] data = text.getBytes(StandardCharsets.UTF_8);
        String hash = sha.hash(data);
        System.out.println("hash = " + hash);

//        String hex = "ba66dcc";
        int i = Integer.parseInt(hash.substring(0,7), 16);
        String bin = Integer.toBinaryString(i);
        System.out.println(bin);


        ECDSA ecceg = new ECDSA(1,6,11);
        ecceg.calculateN();

        String binN = Integer.toBinaryString(ecceg.getN());
        int e = Integer.parseInt(bin.substring(0, binN.length()), 2);
        System.out.println(e);

//        String text2 = "Dhe binary system is a numerical system that functions virtually identically to the decimal number system that people are likely more familiar with. While the decimal number system uses the number 10 as its base, the binary system uses 2.";
//        byte[] data2 = text2.getBytes(StandardCharsets.UTF_8);
//        String hash2 = sha.hash(data2);
//        int i2 = Integer.parseInt(hash2.substring(0,7), 16);
//        String bin2 = Integer.toBinaryString(i2);
//        int e2 = Integer.parseInt(bin2.substring(0, binN.length()), 2);

        Point pubKey = ecceg.createPubKey(5);
        ArrayList<Integer> signasal = new ArrayList<>();
        signasal.add(8);
        signasal.add(10);
        System.out.println("pubkey: " + pubKey.getX() + "," + pubKey.getY());
        ArrayList<Integer> signature = ecceg.generateSignature(e, 7);
        System.out.println(signature.get(0).intValue() + "," + signature.get(1).intValue());
//        System.out.println(ecceg.getN());

        boolean accepted = ecceg.verifSignature(e, signature, pubKey);
        System.out.println(accepted);

//        // testing with file (txt)
//        String file_name = new String("text.txt");
//        Path path_file = Paths.get(file_name);
//        try {
//            byte[] file_data = Files.readAllBytes(path_file);
//            System.out.println("hash = " + sha.hash(file_data) + " " + file_name);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

//        // testing with file (jpg)
//        String file_name_2 = new String("200.jpg");
//        Path path_file_2 = Paths.get(file_name_2);
//        try {
//            byte[] file_data_2 = Files.readAllBytes(path_file_2);
//            System.out.println("hash = " + sha.hash(file_data_2) + " " + file_name_2);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


    }
}

