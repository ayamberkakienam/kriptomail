package com.kriptografi.kriptomail.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.EditText;

import com.kriptografi.ecdsa.src.ECDSA;
import com.kriptografi.ecdsa.src.Point;
import com.kriptografi.ecdsa.src.SHA;
import com.kriptografi.encrypt.src.CipherFeedBack;
import com.kriptografi.kriptomail.Config;
import com.kriptografi.kriptomail.R;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import static com.kriptografi.kriptomail.db.Mail.MAIL_TYPE_OUT;

public class WriteMailActivity extends AppCompatActivity {

    public static final String EXTRA_NEW_MAIL =
            "com.kriptografi.kriptomail.NEW_MAIL";

    private String mNewSenderAddress;
    private EditText mNewRecipientAddress;
    private EditText mNewSubject;
    private EditText mNewContent;
    private String mNewSignature;
    private final Integer mNewType = MAIL_TYPE_OUT;

    private CheckBox check_encryption;
    private CheckBox check_signature;
    private EditText mNewKey;

    private CipherFeedBack cfb;
    private SHA sha;
    private ECDSA ecdsa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_write_mail);

        cfb = new CipherFeedBack();
        sha = new SHA();
        ecdsa = new ECDSA(1,6,11);

        mNewSenderAddress = Config.USER_EMAIL; //  TODO: Implement internal storage to store user's email address.
        mNewRecipientAddress = findViewById(R.id.new_mail_recipient);
        mNewSubject = findViewById(R.id.new_mail_subject);
        mNewContent = findViewById(R.id.new_mail_content);
        mNewSignature = "";  //  TODO: Pass the signature function here (and change mNewSignature's type).

        check_encryption = findViewById(R.id.new_mail_check_encryption);
        check_signature = findViewById(R.id.new_mail_check_signature);
        mNewKey = findViewById(R.id.new_mail_key);

        final Button button = findViewById(R.id.new_mail_button_send);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent replyIntent = new Intent();
                //  TODO: Check if empty NonNull attribute exists.

                String[] data = {
                        mNewSenderAddress,
                        mNewRecipientAddress.getText().toString(),
                        mNewSubject.getText().toString(),
                        mNewContent.getText().toString(),
                        mNewSignature,
                        mNewType.toString()
                };


                if (check_encryption.isChecked()) {
                    data[3] = cfb.callCipherFeedBack(data[3], mNewKey.getText().toString(), "encrypt");
                }

                if (check_signature.isChecked()) {
                    byte[] bdata = data[3].getBytes(StandardCharsets.UTF_8);
                    String hash = sha.hash(bdata);

                    int i = Integer.parseInt(hash.substring(0,7), 16);
                    String bin = Integer.toBinaryString(i);

                    ecdsa.calculateN();

                    String binN = Integer.toBinaryString(ecdsa.getN());
                    int e = Integer.parseInt(bin.substring(0, binN.length()), 2);

                    Point pubKey = ecdsa.createPubKey(5);

                    ArrayList<Integer> signature = ecdsa.generateSignature(e, 7);

                    String newSignature = "<ds>(" + signature.get(0).toString() + "," + signature.get(1).toString() + ")</ds>";

                    data[4] = newSignature;
                }

                //  Concatenate signature to content
                String contentText = data[3] + "\n\n" + data[4];
                sendEmail(data[1], data[2], contentText);    //  TODO: This function call trigger error and will result in activity finished without waiting for the email to be sent. Fix it later.

                replyIntent.putExtra(EXTRA_NEW_MAIL, data);
                setResult(RESULT_OK, replyIntent);
                finish();
            }
        });
    }

    private void sendEmail(String recipient, String subject, String content) {
        SendMail sm = new SendMail(this, recipient, subject, content);
        sm.execute();
    }
}
