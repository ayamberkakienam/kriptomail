package com.kriptografi.kriptomail.activity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.kriptografi.encrypt.src.Main;
import com.kriptografi.kriptomail.R;
import com.kriptografi.kriptomail.db.Mail;
import com.kriptografi.kriptomail.db.MailViewModel;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

        public static final int MAIN_NEW_MAIL_REQUEST_CODE = 1;
        public static final String EXTRA_VIEW_MAIL = "com.kriptografi.kriptomail.VIEW_MAIL";

        private MailViewModel mMailViewModel;
        private List<Mail> mailList = new ArrayList<>();
        private RecyclerView recyclerView;
        private MailListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Recycler View (inbox & sent mail)
        recyclerView = findViewById(R.id.mail_list_recycler);

        mAdapter = new MailListAdapter(mailList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Mail mail = mailList.get(position);

                String address = (mail.getType() == Mail.MAIL_TYPE_IN) ? mail.getSenderAddress() : mail.getRecipientAddress();
                String subject = mail.getSubject();
                String content = mail.getContent();
                String signature = mail.getSignature();

                String[] data = {address, subject, content, signature};

                Intent intent = new Intent(MainActivity.this, ViewMailActivity.class);
                intent.putExtra(EXTRA_VIEW_MAIL, data);
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        recyclerView.setAdapter(mAdapter);

        mMailViewModel = ViewModelProviders.of(this).get(MailViewModel.class);
        mMailViewModel.getAllMails().observe(this, new Observer<List<Mail>>() {
            @Override
            public void onChanged(@Nullable List<Mail> mail) {
                // Update the cached copy of the mails in adapter.
                mAdapter.setMails(mail);
                mailList = mail;
            }
        });

        // Floating Button (write new)
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent intent = new Intent(MainActivity.this, WriteMailActivity.class);
                startActivityForResult(intent, MAIN_NEW_MAIL_REQUEST_CODE);
            }
        });

        // Navigation Drawer (navigation)
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        //  Main View
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_inbox) {

        } else if (id == R.id.nav_sent_mail) {

        } else if (id == R.id.nav_ppkey) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == MAIN_NEW_MAIL_REQUEST_CODE && resultCode == RESULT_OK) {
            String[] mailData = data.getStringArrayExtra(WriteMailActivity.EXTRA_NEW_MAIL);

            Mail mail = new Mail(
              mailData[0],
              mailData[1],
              mailData[2],
              mailData[3],
              mailData[4],
              Integer.parseInt(mailData[5])
            );
            mMailViewModel.insert(mail);
        } else {
            Toast.makeText(
                    getApplicationContext(),
                    "Mail not saved",   //  TODO: Change this when unnecessary.
                    Toast.LENGTH_LONG
            ).show();
        }
    }

}
