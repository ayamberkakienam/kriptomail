package com.kriptografi.kriptomail.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.kriptografi.kriptomail.Config;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendMail extends AsyncTask {

    private Context mContext;
    private Session mSession;

    private String mRecipientAddress;
    private String mSubject;
    private String mContent;
    private String mSignature;

    private ProgressDialog mProgressDialog;

    public SendMail(Context context, String recipientAddress, String subject, String content) {
        this.mContext = context;
        this.mRecipientAddress = recipientAddress;
        this.mSubject = subject;
        this.mContent = content;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mProgressDialog = ProgressDialog.show(mContext, "Sending e-mail", "Please wait...", false, false);
    }

    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        mProgressDialog.dismiss();
        Toast.makeText(mContext,"Message Sent",Toast.LENGTH_LONG).show();
    }

    @Override
    protected Void doInBackground(Object[] objects) {
        Properties props = new Properties();

        props.put("mail.smtp.host", Config.SMTP_HOST);
        props.put("mail.smtp.socketFactory.port", Config.SMTP_PORT);
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", Config.SMTP_PORT);

        mSession = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication () {
                    return new PasswordAuthentication(Config.USER_EMAIL, Config.USER_PASSWORD);
                }
        });

        try {
            MimeMessage mm = new MimeMessage(mSession);

            mm.setFrom(new InternetAddress(Config.USER_EMAIL));
            mm.addRecipient(Message.RecipientType.TO, new InternetAddress(mRecipientAddress));
            mm.setSubject(mSubject);
            mm.setText(mContent);

            Transport.send(mm);
        } catch (MessagingException e) {
            e.printStackTrace();
        }

        return null;
    }
}
