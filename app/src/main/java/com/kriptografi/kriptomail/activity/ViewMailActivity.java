package com.kriptografi.kriptomail.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.kriptografi.kriptomail.R;

import static com.kriptografi.kriptomail.activity.MainActivity.EXTRA_VIEW_MAIL;

public class ViewMailActivity extends AppCompatActivity {

    private TextView mSenderAddress;
    private TextView mSubject;
    private TextView mContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_mail);

        mSenderAddress = findViewById(R.id.mail_sender);
        mSubject = findViewById(R.id.mail_subject);
        mContent = findViewById(R.id.mail_content);

        Intent intent = getIntent();
        String[] data = intent.getStringArrayExtra(EXTRA_VIEW_MAIL);

        String senderAddress = data[0]; //  TODO: FETCH INFORMATION FROM INTERNAL STORAGE
        String subject = data[1];
        String content = data[2] + "\n\n" + data[3];

        mSenderAddress.setText(senderAddress);
        mSubject.setText(subject);
        mContent.setText(content);
    }
}
