package com.kriptografi.kriptomail.activity;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kriptografi.kriptomail.R;
import com.kriptografi.kriptomail.db.Mail;

import java.util.List;

public class MailListAdapter extends RecyclerView.Adapter<MailListAdapter.MailViewHolder> {

    private List<Mail> mMails;


    public class MailViewHolder extends RecyclerView.ViewHolder {

        public TextView subject, recipient;

        private MailViewHolder(View itemView) {
            super(itemView);
            subject = itemView.findViewById(R.id.mail_item_subject);
            recipient = itemView.findViewById(R.id.mail_item_recipient);
        }

    }

    public MailListAdapter(List<Mail> mailList) {
        this.mMails = mailList;
    }

    @NonNull
    @Override
    public MailViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.mail_list_row, viewGroup, false);

        return new MailViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MailViewHolder mailViewHolder, int i) {
        if (mMails != null) {
            Mail current = mMails.get(i);
            mailViewHolder.subject.setText(current.getSubject());
            mailViewHolder.recipient.setText(current.getRecipientAddress());
        }
    }

    void setMails(List<Mail> mails) {
        mMails = mails;
        notifyDataSetChanged();
    }

    // getItemCount() is called many times, and when it is first called,
    // mWords has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if(mMails != null)
            return mMails.size();
        else return 0;
    }

}
