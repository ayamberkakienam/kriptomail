package com.kriptografi.kriptomail.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.kriptografi.kriptomail.Config;
import com.kriptografi.kriptomail.db.Mail;
import com.kriptografi.kriptomail.db.MailDao;

import java.io.IOException;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;

public class ImportMail extends AppCompatActivity {

    private Folder mInbox;
    private Message[] mMails;
    private MailDao mMailDao;
    private Session mSession;
    private Store mStore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void fetchMails() {
        Properties props = System.getProperties();
        props.setProperty("mail.store.protocol", "imaps");

        mSession = Session.getDefaultInstance(props, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(Config.USER_EMAIL, Config.USER_PASSWORD);
            }
        });

        try {
            mStore = mSession.getStore("imaps");
            mStore.connect(Config.SMTP_HOST, Config.USER_EMAIL, Config.USER_PASSWORD);

            mInbox = mStore.getFolder("inbox");
            mInbox.open(Folder.READ_WRITE);
            mMails = mInbox.getMessages();
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    protected void storeMails() throws MessagingException, IOException {
        String senderAddress;
        String recipientAddress;
        String subject;
        String content;
        String signature = "";
        Integer type = Mail.MAIL_TYPE_IN;

        for (Message mail: mMails) {
            senderAddress = mail.getFrom().toString();
            recipientAddress = Config.USER_EMAIL;
            subject = mail.getSubject().toString();
            content = mail.getContent().toString();

            Mail iMail = new Mail(
                    senderAddress,
                    recipientAddress,
                    subject,
                    content,
                    signature,
                    type);
            mMailDao.insert(iMail);
        }
    }

    protected String fetchSignature() {
        return "something";
    }

}
