package com.kriptografi.kriptomail.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "mails")
public class Mail {

    public static final Integer MAIL_TYPE_IN = 0;
    public static final Integer MAIL_TYPE_OUT = 1;

    @NonNull
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private Integer id;
    @NonNull
    @ColumnInfo(name = "from")
    private String senderAddress;
    @NonNull
    @ColumnInfo(name = "to")
    private String recipientAddress;
    @NonNull
    @ColumnInfo(name = "subject")
    private String subject;
    @ColumnInfo(name = "content")
    private String content;
    @ColumnInfo(name = "signature")
    private String signature;
    @NonNull
    @ColumnInfo(name = "type")
    private Integer type;

    // TODO: Create "Date" attribute
    // TODO: Create TypeConverter for Date (https://developer.android.com/training/data-storage/room/referencing-data)

    public Mail(
        @NonNull String senderAddress,
        @NonNull String recipientAddress,
        @NonNull String subject,
        String content,
        String signature,
        @NonNull Integer type
    ) {
        this.senderAddress = senderAddress;
        this.recipientAddress = recipientAddress;
        this.subject = subject;
        this.content = content;
        this.signature = signature;
        this.type = type;
    }

    @NonNull
    public Integer getId() {
        return id;
    }

    public void setId(@NonNull Integer id) {
        this.id = id;
    }

    @NonNull
    public String getSenderAddress() {
        return senderAddress;
    }

    public void setSenderAddress(@NonNull String senderAddress) {
        this.senderAddress = senderAddress;
    }

    @NonNull
    public String getRecipientAddress() {
        return recipientAddress;
    }

    public void setRecipientAddress(@NonNull String recipientAddress) {
        this.recipientAddress= recipientAddress;
    }

    @NonNull
    public String getSubject() {
        return subject;
    }

    public void setSubject(@NonNull String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    @NonNull
    public Integer getType() {
        return type;
    }

    public void setType(@NonNull Integer type) {
        this.type = type;
    }

}