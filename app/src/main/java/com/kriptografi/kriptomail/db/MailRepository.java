package com.kriptografi.kriptomail.db;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.List;

public class MailRepository {

    private MailDao mMailDao;
    private LiveData<List<Mail>> mMails;
    private LiveData<List<Mail>> mInboxMails;
    private LiveData<List<Mail>> mSentMails;

    MailRepository(Application application) {
        MailRoomDatabase db = MailRoomDatabase.getDatabase(application);
        mMailDao = db.mailDao();
        mMails = mMailDao.getAll();
        mInboxMails = mMailDao.getAllIn();
        mSentMails = mMailDao.getAllOut();
    }

    LiveData<List<Mail>> getAllMails() {
        return mMails;
    }
    LiveData<List<Mail>> getAllInboxMails() {
        return mInboxMails;
    }
    LiveData<List<Mail>> getAllSentMails() {
        return mSentMails;
    }

    public void insert(Mail mail) {
        new insertAsyncTask(mMailDao).execute(mail);
    }

    private static class insertAsyncTask extends AsyncTask<Mail, Void, Void> {

        private  MailDao asyncTaskDao;

        insertAsyncTask(MailDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Mail... mail) {
            asyncTaskDao.insert(mail[0]);
            return null;
        }
    }

}
