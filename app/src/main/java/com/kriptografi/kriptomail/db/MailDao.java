package com.kriptografi.kriptomail.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface MailDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Mail mail);

    @Query("SELECT * FROM mails ORDER BY id DESC")
    LiveData<List<Mail>> getAll();

    @Query("SELECT * FROM mails WHERE type='0' ORDER BY id DESC")
    LiveData<List<Mail>> getAllIn();

    @Query("SELECT * FROM mails WHERE type='1' ORDER BY id DESC")
    LiveData<List<Mail>> getAllOut();

    @Delete
    void delete(Mail mail);

    @Delete
    void delete(Mail ... mail);

    @Query("DELETE FROM mails")
    void deleteAll();
}