package com.kriptografi.kriptomail.db;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

public class MailViewModel extends AndroidViewModel {

    private MailRepository mRepository;
    private LiveData<List<Mail>> mMails;
    private LiveData<List<Mail>> mInboxMails;
    private LiveData<List<Mail>> mSentMails;

    public MailViewModel(@NonNull Application application) {
        super(application);
        mRepository = new MailRepository(application);
        mMails = mRepository.getAllMails();
        mInboxMails = mRepository.getAllInboxMails();
        mSentMails = mRepository.getAllSentMails();
    }

    public LiveData<List<Mail>> getAllMails() {
        return mMails;
    }
    public LiveData<List<Mail>> getAllInboxMails() {
        return mInboxMails;
    }
    public LiveData<List<Mail>> getAllSentMails() {
        return mSentMails;
    }

    public void insert(Mail mail) {
        mRepository.insert(mail);
    }

}
