package com.kriptografi.kriptomail.db;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import static com.kriptografi.kriptomail.db.Mail.MAIL_TYPE_IN;
import static com.kriptografi.kriptomail.db.Mail.MAIL_TYPE_OUT;

@Database(entities = { Mail.class }, version = 2, exportSchema = false)
public abstract class MailRoomDatabase extends RoomDatabase {

    public abstract MailDao mailDao();

    private static MailRoomDatabase INSTANCE;
    private static RoomDatabase.Callback mRoomDatabaseCallback = new RoomDatabase.Callback() {

        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);
            new PopulateDbAsync(INSTANCE).execute();
        }
    };

    public static MailRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (MailRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            MailRoomDatabase.class, "kriptomail")
                            .fallbackToDestructiveMigration()
                            //  .addCallback(mRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    /*
    * Populate the database in the background.
    * */
    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final MailDao mDao;
        String[] sender = {
                "test1@gmail.com",
                "ttet2@yahoo.com"
        };
        String[] recipient = {
                "t_test1@gmail.com",
                "t_ttet2@yahoo.com"
        };
        String[] subject = {
                "test subject 1",
                "test subject 2"
        };
        String[] content = {
                "test 1 UWAWAWAAWAWAWAWA",
                "test 2 hehehe"
        };
        String[] signature = {
                "sign1 PASD123P",
                "sign2 12UOJ32K"
        };
        Integer[] type = {
                MAIL_TYPE_IN,
                MAIL_TYPE_OUT
        };

        private PopulateDbAsync(MailRoomDatabase db) {
            mDao = db.mailDao();
        }


        @Override
        protected Void doInBackground(Void... voids) {
            //  Clean the database every time.
            //  mDao.deleteAll();

            for (int i = 0; i <= sender.length-1; i++) {
                Mail mail = new Mail(sender[i], recipient[i], subject[i], content[i], signature[i], type[i]);
                mDao.insert(mail);
            }

            return null;
        }
    }

    static final Migration MIGRATION_1_2 = new Migration(1, 2) {

        @Override
        public void migrate(SupportSQLiteDatabase db) {

        };
    };

}
