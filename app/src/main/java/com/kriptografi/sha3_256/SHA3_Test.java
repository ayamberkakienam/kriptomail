package com.kriptografi.sha3_256;

import android.os.Build;
import android.support.annotation.RequiresApi;

import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.Files;

public class SHA3_Test{
    @RequiresApi(api = Build.VERSION_CODES.O)
    public static void main(String[] args) {
        SHA3 SHA3 = new SHA3();

        // testing with string
        byte[] data = "cobain".getBytes(StandardCharsets.UTF_8);
        System.out.println("hash = " + SHA3.hash(data));

        // testing with file (txt)
        String file_name = "text.txt";
        Path path_file = Paths.get(file_name);
        try {
            byte[] file_data = Files.readAllBytes(path_file);
            System.out.println("hash = " + SHA3.hash(file_data) + " " + file_name);            
        } catch (Exception e) {
            e.printStackTrace();
        }

        // testing with file (jpg)
        String file_name_2 = "200.jpg";
        Path path_file_2 = Paths.get(file_name_2);
        try {
            byte[] file_data_2 = Files.readAllBytes(path_file_2);
            System.out.println("hash = " + SHA3.hash(file_data_2) + " " + file_name_2);            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
