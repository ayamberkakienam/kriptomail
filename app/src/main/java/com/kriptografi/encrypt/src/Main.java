package com.kriptografi.encrypt.src;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        General gen = new General();
//        System.out.println("Hello!");
//        ArrayList<Integer> integers = new ArrayList<Integer> (Arrays.asList(
//                new Integer(1), new Integer(2), new Integer(3), new Integer(4)
//        ));
//        ArrayList<Integer> bits = gen.integersToBit(integers);
//        System.out.println(bits);
//        ArrayList<Integer> ints = gen.bitToIntegers(bits, 8);
//        System.out.println(ints);

//        Keygen keygen = new Keygen(16, 32);
//        ArrayList<Integer> adjustedKey = keygen.adjustKeyLength("hello");
//        System.out.println(adjustedKey);
//
//        String hex = Integer.toHexString(128);
//        System.out.println(hex);
//
//        System.out.println(1 << 4);
//
//        int a = 5/3;
//        System.out.println(a);

        String[][] _SBOX = {
                {"63", "7c", "77", "7b", "f2", "6b", "6f", "c5", "30", "01", "67", "2b", "fe", "d7", "ab", "76"},
                {"ca", "82", "c9", "7d", "fa", "59", "47", "f0", "ad", "d4", "a2", "af", "9c", "a4", "72", "c0"},
                {"b7", "fd", "93", "26", "36", "3f", "f7", "cc", "34", "a5", "e5", "f1", "71", "d8", "31", "15"},
                {"04", "c7", "23", "c3", "18", "96", "05", "9a", "07", "12", "80", "e2", "eb", "27", "b2", "75"},
                {"09", "83", "2c", "1a", "1b", "6e", "5a", "a0", "52", "3b", "d6", "b3", "29", "e3", "2f", "84"},
                {"53", "d1", "00", "ed", "20", "fc", "b1", "5b", "6a", "cb", "be", "39", "4a", "4c", "58", "cf"},
                {"d0", "ef", "aa", "fb", "43", "4d", "33", "85", "45", "f9", "02", "7f", "50", "3c", "9f", "a8"},
                {"51", "a3", "40", "8f", "92", "9d", "38", "f5", "bc", "b6", "da", "21", "10", "ff", "f3", "d2"},
                {"cd", "0c", "13", "ec", "5f", "97", "44", "17", "c4", "a7", "7e", "3d", "64", "5d", "19", "73"},
                {"60", "81", "4f", "dc", "22", "2a", "90", "88", "46", "ee", "b8", "14", "de", "5e", "0b", "db"},
                {"e0", "32", "3a", "0a", "49", "06", "24", "5c", "c2", "d3", "ac", "62", "91", "95", "e4", "79"},
                {"e7", "c8", "37", "6d", "8d", "d5", "4e", "a9", "6c", "56", "f4", "ea", "65", "7a", "ae", "08"},
                {"ba", "78", "25", "2e", "1c", "a6", "b4", "c6", "e8", "dd", "74", "1f", "4b", "bd", "8b", "8a"},
                {"70", "3e", "b5", "66", "48", "03", "f6", "0e", "61", "35", "57", "b9", "86", "c1", "1d", "9e"},
                {"e1", "f8", "98", "11", "69", "d9", "8e", "94", "9b", "1e", "87", "e9", "ce", "55", "28", "df"},
                {"8c", "a1", "89", "0d", "bf", "e6", "42", "68", "41", "99", "2d", "0f", "b0", "54", "bb", "16"}
        };

//        System.out.println(Integer.parseInt(_SBOX[0][2],16));
//        System.out.println(gen.hexSize(255));
//        System.out.println(gen.invertBits(256));
//        int a = 3;
//        String bytes = Integer.toBinaryString(a);
//        System.out.println(bytes);

//        Mutator mutator = new Mutator();
//        System.out.println(mutator.mutateShift(128,1));
//        String hex = "ab";
//        int integers = Integer.parseInt(hex, 16);
//        System.out.println(integers);
//        System.out.println(Integer.toBinaryString(integers));
//        System.out.println((int) 'a');
//
//        String a = "kucingmakansabun";
//        ArrayList<Character> listchar= gen.stringToListChar(a);
//        System.out.println(listchar);
//        System.out.println(gen.listCharToInteger(listchar));

//        Integer[][] testInteger = new Integer[4][8];
//        for (int i = 0; i < 4; i++) {
//            for (int j = 0; j < 8; j++) {
//                testInteger[i][j] = j;
//                System.out.printf(testInteger[i][j].toString());
//            }
//            System.out.println();
//        }

        ArrayList<ArrayList<Integer>> listint = new ArrayList<>();
        ArrayList<Integer> int1 = new ArrayList<Integer> (Arrays.asList(
                new Integer(255), new Integer(250), new Integer(3), new Integer(4),
                new Integer(1), new Integer(2), new Integer(3), new Integer(4),
                new Integer(255), new Integer(250), new Integer(3), new Integer(4),
                new Integer(1), new Integer(2), new Integer(3), new Integer(4)
        ));
//        ArrayList<Integer> int2 = new ArrayList<Integer> (Arrays.asList(
//                new Integer(5), new Integer(6), new Integer(7), new Integer(8)
//        ));
//        listint.add(int1);
//        listint.add(int2);
//        System.out.println(listint);

//        General gen = new General();
//        Keygen keygen = new Keygen(16,16);
//        String key = "kucing1234";
//        ArrayList<ArrayList<Integer>> listkey = keygen.callKeygen(key);
//        System.out.println(listkey);
//
//        ArrayList<ArrayList<Integer>> temp = gen.chunkList(int1, 1);
//        System.out.println(temp);
//
//        System.out.println(int1.addAll(int1));

//        FourSevenFour f = new FourSevenFour();
//        String key = "kucing";
//        ArrayList<Integer> outenc = f.callFourSevenFour(int1, key, "encrypt");
//        System.out.println(outenc);
//        ArrayList<Integer> outdec = f.callFourSevenFour(outenc, key, "decrypt");
//        System.out.println(outdec);

        String text = "Hello";
        String keyEx = "helo";
        CipherFeedBack cfb = new CipherFeedBack();
        System.out.println(text);
        String cipher = cfb.callCipherFeedBack(text, keyEx, "encrypt");
        System.out.println(cipher);
        String plain = cfb.callCipherFeedBack(cipher, keyEx, "decrypt");
        System.out.println(plain);

        System.out.println(Integer.parseInt("ff", 16));
        System.out.println(Integer.toHexString(255));

//        ArrayList<String> hex = gen.listIntToListHexString(int1);
//        System.out.println(hex);
//        System.out.println(gen.listHexStringToListInt(hex));

    }
}
