package com.kriptografi.encrypt.src;

import java.math.BigInteger;
import java.util.ArrayList;

public class Keygen {

    private int lenBlock;
    private int iterate;

    public Keygen(int lenBlock, int iterate) {
        this.lenBlock = lenBlock;
        this.iterate = iterate;
    }

    public int getLenBlock() {
        return lenBlock;
    }

    public void setLenBlock(int lenBlock) {
        this.lenBlock = lenBlock;
    }

    public int getIterate() {
        return iterate;
    }

    public void setIterate(int iterate) {
        this.iterate = iterate;
    }

    public ArrayList<Integer> adjustKeyLength(String keyEx) {
        General gen = new General();
        ArrayList<Character> strKeyEx = gen.stringToListChar(keyEx);
        ArrayList<Integer> listKeyEx = gen.listCharToListInteger(strKeyEx);
        ArrayList<Integer> paddingresult = listKeyEx;
        int keyExSize = keyEx.length();

        if (keyExSize < getLenBlock()) {
            for (int i=0; i < (getLenBlock()-keyExSize); i++) {
                paddingresult.add((int) keyEx.charAt(i % keyExSize));
            }
        }

        return paddingresult;
    }

    public ArrayList<ArrayList<Integer>> callKeygen(String keyEx) {
        ArrayList<ArrayList<Integer>> result = new ArrayList<>();
        General gen = new General();
        Mutator mutator = new Mutator();
        ArrayList<Integer> listKeyEx = adjustKeyLength(keyEx);

        int inc = 0;
        int modSum = 0;
        ArrayList<Integer> listKey;
        ArrayList<Integer> subListKey;
        while (inc < getIterate()) {
            modSum = BigInteger.valueOf(gen.sumListInteger(listKeyEx)).mod(BigInteger.valueOf(3)).intValue();
            listKey = mutator.callMutator(listKeyEx, modSum, modSum);
            subListKey = gen.getFirstHalfListInteger(listKey); //Odd
            result.add(subListKey);
            subListKey = gen.getLastHalfListInteger(listKey);  //Even
            result.add(subListKey);
            listKeyEx = listKey;
            inc+=1;
        }
        return result;
    }
}
