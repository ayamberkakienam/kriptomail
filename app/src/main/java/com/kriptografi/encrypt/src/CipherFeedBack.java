package com.kriptografi.encrypt.src;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Random;

public class CipherFeedBack {

    private int lenBlock;

    public CipherFeedBack() {
        this.lenBlock = 16;
    }

    public int getLenBlock() {
        return lenBlock;
    }

    public void setLenBlock(int lenBlock) {
        this.lenBlock = lenBlock;
    }

    public ArrayList<Integer> preprocessBlock(String text) {
        General gen = new General();
        ArrayList<Character> listchar = gen.stringToListChar(text);
        ArrayList<Integer> listkey = gen.listCharToListInteger(listchar);
        return listkey;
    }

    public ArrayList<Integer> preprocessBlockDecrypt(String text) {
        General gen = new General();
        ArrayList<Character> listchar = gen.stringToListChar(text);
        ArrayList<Integer> result = new ArrayList<>();
        for (int i = 0; i < listchar.size(); i++) {
            StringBuilder temphex = new StringBuilder();
            if (BigInteger.valueOf(i).mod(BigInteger.valueOf(3)).intValue() == 1) {
                temphex.append(listchar.get(i-1));
                temphex.append(listchar.get(i));
                String temp = temphex.toString();
                result.add(Integer.parseInt(temp, 16));
            }
        }
        return result;
    }
    public int getSeedNumber(String keyEx) {
        ArrayList<Integer> listkey = preprocessBlock(keyEx);

        int seedNumber = 0;
        for (int i = 0; i < listkey.size(); i++) {
            seedNumber += listkey.get(i);
        }

        return seedNumber;
    }

    public String callCipherFeedBack(String text, String keyEx, String mode) {
        General gen = new General();
        ArrayList<Integer> initialvector = new ArrayList<>();
        FourSevenFour f = new FourSevenFour();
        Random random = new Random();
        random.setSeed(getSeedNumber(keyEx));
        String result = "";

        for (int i = 0; i < 16; i++) {
            int r = BigInteger.valueOf(random.nextInt()).mod(BigInteger.valueOf(256)).intValue();
            initialvector.add(r);
        }
//        System.out.println(initialvector);
        ArrayList<String> listresult;
        if (mode == "encrypt") {
            ArrayList<Integer> listblock = preprocessBlock(text);
            int inc = 0;
            ArrayList<Integer> listcipher = new ArrayList<>();
            ArrayList<Integer> listencrypt;
            ArrayList<Integer> queue = initialvector;
            while (inc < listblock.size()) {
                listencrypt = f.callFourSevenFour(queue, keyEx, "encrypt");
                listcipher.add(listencrypt.get(0) ^ listblock.get(inc));
                queue.add(listcipher.get(inc));
                queue.remove(0);
                inc+=1;
            }

            listresult = gen.listIntToListHexString(listcipher);
            result = gen.listHexStringToString(listresult);

        } else if (mode == "decrypt") {
            ArrayList<Integer> listblock = preprocessBlockDecrypt(text);
            int inc = 0;
            ArrayList<Integer> listplain = new ArrayList<>();
            ArrayList<Integer> listdecrypt;
            ArrayList<Integer> queue = initialvector;
            while (inc < listblock.size()) {
                listdecrypt = f.callFourSevenFour(queue, keyEx, "encrypt");
                listplain.add(listdecrypt.get(0) ^ listblock.get(inc));
                queue.add(listblock.get(inc));
                queue.remove(0);
                inc+=1;
            }

            result = gen.listIntToString(listplain);
        }

        return result;
    }
}
