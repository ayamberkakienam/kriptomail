package com.kriptografi.encrypt.src;

import java.math.BigInteger;
import java.util.ArrayList;

public class General {

    public String convertToBinary(int num)
    {
        String str = "";
        for(int i = 0; i < 8; i++) {
            if(num % 2 == 1) {
                str = "1" + str;
            }
            if(num % 2 == 0) {
                str = "0" + str;
            }
            num = num / 2;
        }
        return str;
    }

    public ArrayList<Integer> integersToBit(ArrayList<Integer> n) {
        ArrayList<Integer> listbits = new ArrayList<>();
        String result = "";
        for (int i=0; i<n.size(); i++) {
            result += convertToBinary(n.get(i));
        }

        for (int i=0; i<result.length(); i++) {
            listbits.add(Integer.parseInt(result.substring(i,i+1)));
        }
        return listbits;
    }

    public ArrayList<Integer> bitToIntegers(ArrayList<Integer> bits, int bitsize) {
        ArrayList<Integer> result = new ArrayList<>();
        String tempresult = "";

        for (int i = 0; i < bits.size(); i++) {
            tempresult += Integer.toString(bits.get(i).intValue());
        }

        for (int i=0; i < tempresult.length(); i+= bitsize) {
            result.add(Integer.parseInt(tempresult.substring(i, i+bitsize),2));
        }
        return result;
    }

    public ArrayList<Integer> splitHex(int n, int size) {
        ArrayList<Integer> result = new ArrayList<>();

        int row, column;
        row = n / (1 << (int) Math.pow(2, size));
        column = BigInteger.valueOf((long) n).mod(BigInteger.valueOf((long) (1 << (int) Math.pow(2, size)))).intValue();

        result.add(row);
        result.add(column);

        return result;
    }

    public int hexSize(int n) {
        int i = 1;
        while (n > 15) {
            n = n >> 4;
            i+=1;
        }
        return i;
    }

    public int invertBits(int n) {
        String bits = Integer.toBinaryString(n);
        String result = "";
        for (int i = 0; i < bits.length(); i++) {
            if (bits.charAt(i) == '1') {
                result += '0';
            } else {
                result += '1';
            }
        }
        return Integer.parseInt(result, 2);
    }

    public ArrayList<Integer> listCharToListInteger(ArrayList<Character> listchar) {
        ArrayList<Integer> result = new ArrayList<>();
        for (int i = 0; i < listchar.size(); i++) {
            result.add((int) listchar.get(i));
        }
        return result;
    }

    public ArrayList<Character> listIntegerToListChar(ArrayList<Integer> listint) {
        ArrayList<Character> result = new ArrayList<>();
        for (int i=0; i < listint.size(); i++) {
            result.add((char) listint.get(i).intValue());
        }
        return result;
    }

    public ArrayList<Character> stringToListChar(String str) {
        ArrayList<Character> result = new ArrayList<>();
        for (int i = 0; i < str.length(); i++) {
            result.add(str.charAt(i));
        }
        return result;
    }

    public String listCharToString(ArrayList<Character> listchar) {
        String result = "";
        for (int i=0; i < listchar.size(); i++) {
            result += listchar.get(i);
        }
        return result;
    }

    public String listIntToString(ArrayList<Integer> listint) {
        ArrayList<Character> listchar = listIntegerToListChar(listint);
        String result = listCharToString(listchar);
        return result;
    }

    public String listStringToString(ArrayList<String> liststr) {
        String result = "";
        for (int i=0; i < liststr.size(); i++) {
            result += liststr.get(i);
        }
        return result;
    }

    public String listHexStringToString(ArrayList<String> listhexstr) {
        String result = "";
        for (int i=0; i < listhexstr.size(); i++) {
            result = result + listhexstr.get(i) + " ";
        }
        return result;
    }

    public ArrayList<String> listIntToListHexString(ArrayList<Integer> listint) {
        ArrayList<String> result = new ArrayList<>();
        for(int i = 0; i < listint.size(); i++) {
            if (listint.get(i) < 16) {
                String hex = "0" + Integer.toHexString(listint.get(i));
                result.add(hex);
            } else {
                result.add(Integer.toHexString(listint.get(i)));
            }
        }
        return result;
    }

    public ArrayList<Integer> listHexStringToListInt(ArrayList<String> liststr) {
        ArrayList<Integer> result = new ArrayList<>();
        for (int i = 0; i < liststr.size(); i++) {
            result.add(Integer.parseInt(liststr.get(i), 16));
        }
        return result;
    }

    public long sumListInteger(ArrayList<Integer> listint) {
        long sum = 0;
        for (int i = 0; i < listint.size(); i++) {
            sum += listint.get(i);
        }
        return sum;
    }

    public ArrayList<Integer> getFirstHalfListInteger(ArrayList<Integer> listint) {
        ArrayList<Integer> result = new ArrayList<>();
        for (int i = 0; i < listint.size()/2; i++) {
            result.add(listint.get(i));
        }
        return result;
    }

    public ArrayList<Integer> getLastHalfListInteger(ArrayList<Integer> listint) {
        ArrayList<Integer> result = new ArrayList<>();
        for (int i = listint.size()/2; i < listint.size(); i++) {
            result.add(listint.get(i));
        }
        return result;
    }

    public ArrayList<Integer> emptyList(int size) {
        ArrayList<Integer> result = new ArrayList<>();

        for (int i = 0; i < size; i++) {
            result.add(null);
        }

        return result;
    }

    public ArrayList<ArrayList<Integer>> chunkList(ArrayList<Integer> listint, int chunkSize) {
        ArrayList<ArrayList<Integer>> result = new ArrayList<>();
        for (int i = 0; i < listint.size(); i+=chunkSize){
            ArrayList<Integer> temp = new ArrayList<>();
            int chunk = listint.size() - i*chunkSize;
            int modChunk = 0;
            if (chunk >= 0) {
                modChunk = chunkSize;
            } else {
                modChunk = BigInteger.valueOf(chunk).mod(BigInteger.valueOf(chunkSize)).intValue();
                if (modChunk == 0) {
                    modChunk = chunkSize;
                }
            }
            for (int j = 0; j < modChunk; j++) {
                temp.add(listint.get(i+j));
            }
            result.add(temp);
        }
        return result;
    }
}
