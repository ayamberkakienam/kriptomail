package com.kriptografi.encrypt.src;

import java.math.BigInteger;
import java.security.Key;
import java.util.ArrayList;
import java.util.Arrays;

public class FourSevenFour {

    public static int _BIT_SIZE = 128;
    public static int _INPUT_SIZE = _BIT_SIZE/8;
    public static int _FEISTEL_BLOCK_SIZE = _INPUT_SIZE/2;
    public static ArrayList<Integer> _P_BOX = new ArrayList<Integer> (Arrays.asList(
            new Integer(57),new Integer(49),new Integer(41),new Integer(33),new Integer(25),new Integer(17),new Integer(9),new Integer(1),new Integer(121),new Integer(113),new Integer(105),new Integer(97),new Integer(89),new Integer(81),new Integer(73),new Integer(65),
            new Integer(61),new Integer(53),new Integer(45),new Integer(37),new Integer(29),new Integer(21),new Integer(13),new Integer(5),new Integer(125),new Integer(117),new Integer(109),new Integer(101),new Integer(93),new Integer(85),new Integer(77),new Integer(69),
            new Integer(56),new Integer(48),new Integer(40),new Integer(32),new Integer(24),new Integer(16),new Integer(8),new Integer(0),new Integer(120),new Integer(112),new Integer(104),new Integer(96),new Integer(88),new Integer(80),new Integer(72),new Integer(64),
            new Integer(60),new Integer(52),new Integer(44),new Integer(36),new Integer(28),new Integer(20),new Integer(12),new Integer(4),new Integer(124),new Integer(116),new Integer(108),new Integer(100),new Integer(92),new Integer(84),new Integer(76),new Integer(68),
            new Integer(55),new Integer(47),new Integer(39),new Integer(31),new Integer(23),new Integer(15),new Integer(7),new Integer(127),new Integer(119),new Integer(111),new Integer(103),new Integer(95),new Integer(87),new Integer(79),new Integer(71),new Integer(63),
            new Integer(59),new Integer(51),new Integer(43),new Integer(35),new Integer(27),new Integer(19),new Integer(11),new Integer(3),new Integer(123),new Integer(115),new Integer(107),new Integer(99),new Integer(91),new Integer(83),new Integer(75),new Integer(67),
            new Integer(54),new Integer(46),new Integer(38),new Integer(30),new Integer(22),new Integer(14),new Integer(6),new Integer(126),new Integer(118),new Integer(110),new Integer(102),new Integer(94),new Integer(86),new Integer(78),new Integer(70),new Integer(62),
            new Integer(58),new Integer(50),new Integer(42),new Integer(34),new Integer(26),new Integer(18),new Integer(10),new Integer(2),new Integer(122),new Integer(114),new Integer(106),new Integer(98),new Integer(90),new Integer(82),new Integer(74),new Integer(66)
    ));

    public ArrayList<Integer> adjustInput(ArrayList<Integer> inputblock) {
        General gen = new General();
        return gen.integersToBit(inputblock);
    }

    public ArrayList<Integer> normalizeInput(ArrayList<Integer> inputblock) {
        General gen = new General();
        return gen.bitToIntegers(inputblock, 8);
    }

    public ArrayList<Integer> fFunction(ArrayList<Integer> inputblock, ArrayList<Integer> key) {
        General gen = new General();
        Mutator mutator = new Mutator();
        ArrayList<Integer> outputblock = new ArrayList<>();
        long modSum = 0;
        modSum = BigInteger.valueOf(gen.sumListInteger(key)).mod(BigInteger.valueOf(6)).intValue();
        Integer[][] mutatorChoice = {
                {mutator.TYPE_SBOX, mutator.TYPE_INV, mutator.TYPE_SHIFT},
                {mutator.TYPE_INV, mutator.TYPE_SBOX, mutator.TYPE_SHIFT},
                {mutator.TYPE_SHIFT, mutator.TYPE_INV, mutator.TYPE_SBOX},
                {mutator.TYPE_SBOX, mutator.TYPE_SHIFT, mutator.TYPE_INV},
                {mutator.TYPE_INV, mutator.TYPE_SHIFT, mutator.TYPE_SBOX},
                {mutator.TYPE_SHIFT, mutator.TYPE_SBOX, mutator.TYPE_INV}
        };

        int inc = 0;
        outputblock = inputblock;
        while (inc < mutatorChoice[(int) modSum].length) {
            outputblock = mutator.callMutator(outputblock, mutatorChoice[(int) modSum][inc], (int) modSum);
            inc+=1;
        }

        return outputblock;
    }

    public ArrayList<ArrayList<Integer>> feistelSequence(ArrayList<Integer> leftBlock, ArrayList<Integer> rightBlock, ArrayList<Integer> key) {
        ArrayList<ArrayList<Integer>> result = new ArrayList<>();
        ArrayList<Integer> modBlock = fFunction(rightBlock, key);
        ArrayList<Integer> altLeftBlock = new ArrayList<>();

        for (int i = 0; i < _FEISTEL_BLOCK_SIZE; i++) {
            altLeftBlock.add(leftBlock.get(i) ^ modBlock.get(i));
        }

        result.add(rightBlock);
        result.add(altLeftBlock);
        return result;
    }

    public ArrayList<Integer> initialPermutation(ArrayList<Integer> inputblock) {
        General gen = new General();
        inputblock = adjustInput(inputblock);
        ArrayList<Integer> outputblock = gen.emptyList(_BIT_SIZE);

        int px;
        for (int i = 0; i < _BIT_SIZE; i++) {
            px = _P_BOX.get(i).intValue();
            outputblock.set(i, inputblock.get(px));
        }

        outputblock = normalizeInput(outputblock);
        return outputblock;
    }

    public ArrayList<Integer> initialPermutationInv(ArrayList<Integer> inputblock) {
        General gen = new General();
        inputblock = adjustInput(inputblock);
        ArrayList<Integer> outputblock = gen.emptyList(_BIT_SIZE);

        int px;
        for (int i = 0; i < _BIT_SIZE; i++) {
            px = _P_BOX.get(i).intValue();
            outputblock.set(px, inputblock.get(i));
        }

        outputblock = normalizeInput(outputblock);
        return outputblock;
    }

    public ArrayList<Integer> loop(ArrayList<Integer> inputblock, String keyEx, String mode, int amount) {
        //amount = 32
        General gen = new General();
        ArrayList<Integer> block = initialPermutation(inputblock);
        ArrayList<ArrayList<Integer>> listblock = gen.chunkList(block, _FEISTEL_BLOCK_SIZE);
        ArrayList<Integer> outputblock = new ArrayList<>();

        ArrayList<Integer> leftBlock = listblock.get(0);
        ArrayList<Integer> rightBlock = listblock.get(1);

        Keygen keygen = new Keygen(16,16);
        ArrayList<ArrayList<Integer>> listkey = keygen.callKeygen(keyEx);

        ArrayList<ArrayList<Integer>> nextBlock = new ArrayList<>();
        for (int i=0; i < amount; i++) {
            if (mode == "encrypt") {
                nextBlock = feistelSequence(leftBlock, rightBlock, listkey.get(i));
                leftBlock = nextBlock.get(0);
                rightBlock = nextBlock.get(1);
            } else if (mode == "decrypt") {
                nextBlock = feistelSequence(rightBlock, leftBlock, listkey.get(amount-i-1));
                leftBlock = nextBlock.get(1);
                rightBlock = nextBlock.get(0);
            }
        }

        outputblock = leftBlock;
        for (int i = 0; i < rightBlock.size(); i++) {
            outputblock.add(rightBlock.get(i));
        }

        outputblock = initialPermutationInv(outputblock);

        return outputblock;
    }

    public ArrayList<Integer> callFourSevenFour(ArrayList<Integer> inputblock, String keyEx, String mode) {
        return loop(inputblock, keyEx, mode, 32);
    }

}
